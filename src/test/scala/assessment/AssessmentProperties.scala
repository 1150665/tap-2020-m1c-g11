package assessment

import java.time.{Duration, LocalDateTime, ZoneOffset}

import domain._
import domain.schedule.Schedule
import org.scalacheck.Prop.forAll
import org.scalacheck.{Gen, Properties}

class AssessmentProperties(s: Schedule) extends Properties("Schedule") {

   /** method to create some more readable ids
   * @param t type of Id
   * @return the gen id    */
  def genId (t: String): Gen[String] = for {
    s <- Gen.listOfN(4, Gen.numChar)
    if (!s.isEmpty)
  } yield t + s.mkString

  lazy val gLocalDateTime: Gen[LocalDateTime] = Gen.choose(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC), LocalDateTime.now().plusDays(1)
    .toEpochSecond(ZoneOffset.UTC)).map(LocalDateTime.ofEpochSecond(_,0,ZoneOffset.UTC))

  lazy val gDuration: (Long,Long) => Gen[Duration] = (min,max) =>
    Gen.choose(Duration.ofHours(min).getSeconds,Duration.ofHours(max).getSeconds).map(Duration.ofSeconds(_))

  lazy val gAvailability: Gen[Availability] = for {
    start <- gLocalDateTime
    end <- gDuration(3,7).map(start.plus(_))
    preference <- Gen.chooseNum(1,5)
  } yield Availability(start, end, preference)

  lazy val gExternal: Gen[External] = for {
    id <- genId("E")
    name <- Gen.listOfN(10, Gen.alphaLowerChar).map(_.mkString)
    n <- Gen.chooseNum(2,4)
    availabilities <- Gen.listOfN(n, gAvailability)
  } yield External(id, name, availabilities)

  lazy val gTeacher: Gen[Teacher] = for {
    id <- genId("T")
    name <- Gen.listOfN(10, Gen.alphaLowerChar).map(_.mkString)
    n <- Gen.chooseNum(2,4)
    availabilities <- Gen.listOfN(n, gAvailability)
  } yield Teacher(id, name, availabilities)

  lazy val gResource: Gen[Resource] = Gen.oneOf(gTeacher,gExternal)
  
  val glResources: Gen[List[Resource]] = for {
    n <- Gen.chooseNum(4,7)
    resources <- Gen.listOfN(n, gResource)
  } yield resources

  lazy val gJuror: Resource => Gen[Juror] = resource => for (
    role <- Gen.oneOf[Role](List(Coadviser,Supervisor))
  ) yield Juror(resource.id, role)

  lazy val gPresident: Resource => Gen[Juror] = resource => for (
    role <- Gen.oneOf[Role](List(President))
  ) yield Juror(resource.id, role)

  lazy val gAdviser: Resource => Gen[Juror] = resource => for (
    role <- Gen.oneOf[Role](List(Adviser))
  ) yield Juror(resource.id, role)

  /*val gJury: List[Resource] => Gen[List[Juror]] = resources => for {
    n <- Gen.chooseNum(3,5)
    r <- Gen.pick(n, resources)
    p = List(gPresident(r.head))
    a = List(gAdviser(r.tail.head))
    gens = r.drop(2).map(x => gJuror(x)).toList
    j <- Gen.sequence[List[Juror], Juror](p:::a:::gens)
  } yield j*/

  val gJury: List[Resource] => Gen[List[Juror]] = resources => {
    Gen.pick(4, resources).map{lr =>
      val gJurorList = lr.foldLeft(List[Gen[Juror]]()){
        case (jury, r) =>
          if(jury.size == 0){
            jury :+ gPresident(r)
          } else{
            if(jury.size == 1){
              jury :+ gAdviser(r)
            }
            else{
              jury :+ gJuror(r)
            }
          }
      }
      Gen.sequence[List[Juror], Juror](gJurorList)
    }.sample.get
  }

  lazy val gViva: List[Resource] => Gen[Viva] = resources => for (
    title <- Gen.listOfN(10,Gen.alphaLowerChar).map(_.mkString);
    student <- Gen.listOfN(10,Gen.alphaLowerChar).map(_.mkString);
    jury <- gJury(resources)
  ) yield Viva(title, student, jury)

  val glVivas: List[Resource] => Gen[List[Viva]] = resources => for {
    n <- Gen.chooseNum(2,6)
    vivas <- Gen.listOfN(n, gViva(resources))
  } yield vivas

  property("All the viva must be scheduled in the intervals in which its resources are available") =
    forAll(gDuration(1,3))( duration => {
      forAll(glResources)( resources => {
        forAll(glVivas(resources))( vivas => {
          val scheduledVivas = s.scheduleVivas(vivas, resources, duration)
          //println(scheduledVivas.size)
          if(scheduledVivas.isSuccess){
            scheduledVivas.get.forall{ sv =>
              resources.filter(r => sv.viva.jury.exists(_.resource.equals(r.id))).forall{r =>
                r.availabilities.exists(av => av.finishedBy(sv.availability) || av.contains(sv.availability)
                  || sv.availability.starts(av) || av.equals(sv.availability))
              }
            }
          }else{
            true
          }
        })
      })
    })

  property("One resource cannot be overlapped in two scheduled viva") = {
    forAll(gDuration(1,3))( duration => {
      forAll(glResources)( resources => {
        forAll(glVivas(resources))( vivas => {
          val lsv = s.scheduleVivas(vivas, resources, duration)
          if(lsv.isSuccess){
            resources.forall{ r =>
              val svAvailabilities = lsv.get.filter(_.viva.jury.exists(_.resource.equals(r.id))).map(_.availability)
              val index = 0
              val notOverlapped = true
              svAvailabilities.foldLeft(notOverlapped, index){
                case ((notOverlapped,index),av) =>
                  if(svAvailabilities.patch(index,Nil,1).find(sva => av.finishedBy(sva) || av.contains(sva) || sva.starts(av) || av.equals(sva)).isDefined){
                    (false,index+1)
                  }else{
                    (notOverlapped,index+1)
                  }
              }._1
            }
          }else{
            true
          }
        })
      })
    })
  }

  property("Check all schedules vivas use only duration time") = {
    forAll(gDuration(24,48))( duration => {
      forAll(glResources)( resources => {
        forAll(glVivas(resources))( vivas => {
          val scheduledVivas = s.scheduleVivas(vivas, resources, duration)
          //println(scheduledVivas.size)
          if(scheduledVivas.isSuccess){
            scheduledVivas.get.forall(sv => Duration.between(sv.availability.start, sv.availability.end) == duration)
          }else{
            true
          }
        })
      })
    })
  }

  property("Check number of generated vivas is the same as scheduled ones") = {
    forAll(gDuration(1,3))( duration => {
      forAll(glResources)( resources => {
        forAll(glVivas(resources))( vivas => {
          val scheduledVivas = s.scheduleVivas(vivas, resources, duration)
          if(scheduledVivas.isSuccess){
            scheduledVivas.get.size == vivas.size
          }else{
            true
          }
        })
      })
    })
  }
}
