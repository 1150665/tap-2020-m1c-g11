import java.time.{Duration, LocalDateTime, ZoneOffset}
import java.time.temporal.ChronoUnit

import domain._
import org.scalacheck.Gen
import org.scalacheck.Prop.forAll
import assessment.{AssessmentMS01, AssessmentProperties}

import scala.util.Random

def convertLDiffToLInst(start: LocalDateTime, ld: List[Int]): List[LocalDateTime] =
  ld.foldLeft[(LocalDateTime, List[LocalDateTime])](start, Nil) {
    case ((i, li), d) => val ni = i.plus(d, ChronoUnit.MINUTES)
      (ni, li :+ ni)
  }._2
/**
 * Gerar lista de n instantes temporais (1 a 10) separados entre 10min a 1h
 * @param start
 * @return
 */
def getInstants(start: LocalDateTime): Gen[List[LocalDateTime]] = for {
  n <- Gen.chooseNum(0,9)
  ld <- Gen.listOfN(n, Gen.chooseNum(10, 60)) //
} yield convertLDiffToLInst(start, 0 :: ld)


Gen.listOfN(4, Gen.numChar).sample

def genId (t: String): Gen[String] = for {
  s <- Gen.listOfN(4, Gen.numChar)
  if (!s.isEmpty)
} yield t + s.mkString

lazy val gLocalDateTime: Gen[LocalDateTime] = Gen.choose(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC), LocalDateTime.now().plusDays(1)
  .toEpochSecond(ZoneOffset.UTC)).map(LocalDateTime.ofEpochSecond(_,0,ZoneOffset.UTC))

lazy val gDuration: Gen[Duration] = Gen.choose(Duration.ofHours(1.5.toLong).getSeconds,Duration.ofHours(4).getSeconds).map(Duration.ofSeconds(_))

lazy val gAvailability: Gen[Availability] = for (
  start <- gLocalDateTime;
  end <- gDuration.map(start.plus(_));
  preference <- Gen.choose(1,5)
) yield Availability(start, end, preference)

lazy val gExternal: Gen[External] = for (
  id <- genId("E");
  name <- Gen.listOfN(10,Gen.alphaLowerChar).map(_.mkString);
  availabilities <- Gen.listOfN(2 + new Random().nextInt(2),gAvailability)
) yield External(id, name, availabilities)

lazy val gTeacher: Gen[Teacher] = for (
  id <- genId("T");
  name <- Gen.listOfN(10,Gen.alphaLowerChar).map(_.mkString);
  availabilities <- Gen.listOfN(2 + new Random().nextInt(2),gAvailability)
) yield Teacher(id, name, availabilities)

lazy val gResource: Gen[Resource] = Gen.oneOf(gTeacher,gExternal)
val glResources: Gen[List[Resource]] = Gen.listOfN(4 + new Random().nextInt(7), gResource)

lazy val gJuror: Resource => Gen[Juror] = resource => for (
  role <- Gen.oneOf[Role](List(Coadviser,Supervisor))
) yield Juror(resource.id, role)

lazy val gPresident: Resource => Gen[Juror] = resource => for (
  role <- Gen.oneOf[Role](List(President))
) yield Juror(resource.id, role)

lazy val gAdviser: Resource => Gen[Juror] = resource => for (
  role <- Gen.oneOf[Role](List(Adviser))
) yield Juror(resource.id, role)

val gJury: List[Resource] => Gen[List[Juror]] = resources => {
  Gen.pick(3 + new Random().nextInt(2), resources).map{lr =>
    val gJurorList = lr.foldLeft(List[Gen[Juror]]()){
      case (jury, r) =>
        if(jury.size == 0){
          jury :+ gPresident(r)
        } else{
          if(jury.size == 1){
            jury :+ gAdviser(r)
          }
          else{
            jury :+ gJuror(r)
          }
        }
    }
    Gen.sequence[List[Juror], Juror](gJurorList)
  }.sample.get
}

lazy val gViva: List[Resource] => Gen[Viva] = resources => for (
  title <- Gen.listOfN(10,Gen.alphaLowerChar).map(_.mkString);
  student <- Gen.listOfN(10,Gen.alphaLowerChar).map(_.mkString);
  jury <- gJury(resources)
) yield Viva(title, student, jury)

val glVivas: List[Resource] => Gen[List[Viva]] = resources => {
  Gen.listOfN(2 + new Random().nextInt(4), gViva(resources))
}

val s = AssessmentMS01

val duration = Duration.ofMinutes(90)
val resources = glResources.sample.getOrElse(???)
val nR = resources.size
val vivas = glVivas(resources).sample.getOrElse(???)
val nV = vivas.size
val lsv = s.scheduleVivas(vivas, resources, duration).filter(_.isSuccess).map(_.get)
val nSV = lsv.size

resources.forall(r => lsv.filter(_.viva.jury.exists(_.resource.equals(r.id))).map(_.availability)
  .foldLeft((Set.empty[Availability], false)){
    case ((seen, duplicatesB), cur) => if(seen.exists(_.equals(cur))) (seen, true) else (seen + cur, duplicatesB)
}._2)


val gJury2: List[Resource] => Gen[List[Juror]] = resources => for {
  n <- Gen.chooseNum(3,5)
  j <- Gen.pick(n, resources).map{lr =>
    val gJurorList = lr.foldLeft(List[Gen[Juror]]()){
      case (jury, r) =>
        if(jury.size == 0){
          jury :+ gPresident(r)
        }else{
          if(jury.size == 1){
            jury :+ gAdviser(r)
          }
          else{
            jury :+ gJuror(r)
          }
        }
    }
    Gen.sequence[List[Juror], Juror](gJurorList)
  }.sample.get
} yield j