package domain

import org.scalatest.funsuite.AnyFunSuite

class VivaTest extends AnyFunSuite {

  val viva1 = Viva.fromXml(<viva student="V001" title="Title: V001">
    <president id="T001"/>
    <adviser id="T002"/>
    <coadviser id="E001"/>
  </viva>).getOrElse(???)

  val viva2 = Viva.fromXml(<viva student="V002" title="Title: V002">
      <president id="T002"/>
      <adviser id="T003"/>
      <coadviser id="E002"/>
  </viva>).getOrElse(???)

  val viva3 = Viva.fromXml(<viva student="V003" title="Title: V003">
      <president id="T003"/>
      <adviser id="T001"/>
      <coadviser id="E001"/>
  </viva>).getOrElse(???)

  val viva4 = Viva.fromXml(<viva student="V003" title="Title: V003">
    <president id="T005"/>
    <adviser id="T004"/>
    <coadviser id="E002"/>
  </viva>).getOrElse(???)

  // amountOfVivasWithSharedJury
  test ("amountOfVivasWithSharedJury") {
    val lVivas = List(viva2, viva3, viva4)
    val expected = 2
    val result = viva1.amountOfVivasWithSharedJury(lVivas)
    assert(result === expected)
  }

}
