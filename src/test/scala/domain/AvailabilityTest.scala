package domain

import java.time.Duration

import org.scalatest.funsuite.AnyFunSuite

class AvailabilityTest extends AnyFunSuite {

  val avail1 = Availability.fromXml(<availability start="2020-06-10T18:00:00" end="2020-06-10T19:45:00" preference="1"/>).getOrElse(???)
  val overlappingAvail = Availability.fromXml(<availability start="2020-06-10T18:30:00" end="2020-06-10T19:50:00" preference="1"/>).getOrElse(???)
  val finishedByAvail = Availability.fromXml(<availability start="2020-06-10T19:00:00" end="2020-06-10T19:45:00" preference="1"/>).getOrElse(???)
  val containsAvail = Availability.fromXml(<availability start="2020-06-10T18:10:00" end="2020-06-10T19:00:00" preference="1"/>).getOrElse(???)
  val startsAvail = Availability.fromXml(<availability start="2020-06-10T18:00:00" end="2020-06-11T00:30:00" preference="1"/>).getOrElse(???)
  val equalsAvail = Availability.fromXml(<availability start="2020-06-10T18:00:00" end="2020-06-10T19:45:00" preference="1"/>).getOrElse(???)

  test("create fromXml") {
    assert(Availability.fromXml(<availability start="2020-06-10T18:00:00" end="2020-06-10T19:45:00" preference="-1"/>).isFailure)
    assert(Availability.fromXml(<availability start="2020-06-11T18:00:00" end="2020-06-10T19:45:00" preference="1"/>).isFailure)
    assert(Availability.fromXml(<availability start="2020-06-11T20:00:00" end="2020-06-11T19:45:00" preference="5"/>).isFailure)
  }

  // overlaps
  test("overlaps success") {
    assert(avail1.overlaps(overlappingAvail) === true)
    assert(overlappingAvail.overlaps(avail1) === false)
  }

  test("overlaps failure") {
    val notOverlappingAvail = Availability.fromXml(<availability start="2020-06-10T17:30:00" end="2020-06-10T19:50:00" preference="1"/>).getOrElse(???)
    assert(avail1.overlaps(notOverlappingAvail) === false)
    assert(notOverlappingAvail.overlaps(avail1) === false)
  }

  test("overlaps with duration") {
    val excepted = Some(Availability.fromXml(<availability start="2020-06-10T18:30:00" end="2020-06-10T19:45:00" preference="2"/>).getOrElse(???))
    assert(avail1.overlaps(overlappingAvail, Duration.ofMinutes(30)) === excepted)
    assert(avail1.overlaps(overlappingAvail, Duration.ofMinutes(75)) === excepted)
    assert(avail1.overlaps(overlappingAvail, Duration.ofMinutes(76)) === None)
  }

  // finishedBy
  test("finishedBy success") {
    assert(avail1.finishedBy(finishedByAvail) === true)
    assert(finishedByAvail.finishedBy(avail1) === false)
  }

  test("finishedBy failure") {
    val notFinishedByAvail = Availability.fromXml(<availability start="2020-06-10T17:30:00" end="2020-06-10T19:50:00" preference="1"/>).getOrElse(???)
    assert(avail1.finishedBy(notFinishedByAvail) === false)
    assert(notFinishedByAvail.finishedBy(avail1) === false)
  }

  test("finishedBy with duration") {
    val excepted = Some(Availability.fromXml(<availability start="2020-06-10T19:00:00" end="2020-06-10T19:45:00" preference="2"/>).getOrElse(???))
    assert(avail1.finishedBy(finishedByAvail, Duration.ofMinutes(30)) === excepted)
    assert(avail1.finishedBy(finishedByAvail, Duration.ofMinutes(45)) === excepted)
    assert(avail1.finishedBy(finishedByAvail, Duration.ofMinutes(46)) === None)
  }

  // contains
  test("contains success") {
    assert(avail1.contains(containsAvail) === true)
    assert(containsAvail.contains(avail1) === false)
  }

  test("contains failure") {
    val notContainsAvail = Availability.fromXml(<availability start="2020-06-10T17:30:00" end="2020-06-10T17:50:00" preference="1"/>).getOrElse(???)
    assert(avail1.contains(notContainsAvail) === false)
    assert(notContainsAvail.contains(avail1) === false)
  }

  test("contains with duration") {
    val excepted = Some(Availability.fromXml(<availability start="2020-06-10T18:10:00" end="2020-06-10T19:00:00" preference="2"/>).getOrElse(???))
    assert(avail1.contains(containsAvail, Duration.ofMinutes(30)) === excepted)
    assert(avail1.contains(containsAvail, Duration.ofMinutes(50)) === excepted)
    assert(avail1.contains(containsAvail, Duration.ofMinutes(60)) === None)
  }

  // starts
  test("starts success") {
    assert(avail1.starts(startsAvail) === true)
    assert(startsAvail.starts(avail1) === false)
  }

  test("starts failure") {
    val notStartsAvail = Availability.fromXml(<availability start="2020-06-10T19:00:00" end="2020-06-10T19:50:00" preference="1"/>).getOrElse(???)
    assert(avail1.starts(notStartsAvail) === false)
    assert(notStartsAvail.starts(avail1) === false)
  }

  test("starts with duration") {
    val excepted = Some(Availability.fromXml(<availability start="2020-06-10T18:00:00" end="2020-06-10T19:45:00" preference="2"/>).getOrElse(???))
    assert(avail1.starts(startsAvail, Duration.ofMinutes(95)) === excepted)
    assert(avail1.starts(startsAvail, Duration.ofMinutes(391)) === None)
  }

  // equals
  test("equals success") {
    assert(avail1.equals(equalsAvail) === true)
    assert(equalsAvail.equals(avail1) === true)
  }

  test("equals failure") {
    val notEqualsAvail = Availability.fromXml(<availability start="2020-06-10T19:00:00" end="2020-06-10T19:50:00" preference="1"/>).getOrElse(???)
    assert(avail1.equals(notEqualsAvail) === false)
    assert(notEqualsAvail.equals(avail1) === false)
  }

  test("equals with duration") {
    val excepted = Some(Availability.fromXml(<availability start="2020-06-10T18:00:00" end="2020-06-10T19:45:00" preference="2"/>).getOrElse(???))
    assert(avail1.equals(equalsAvail, Duration.ofMinutes(90)) === excepted)
    assert(avail1.equals(equalsAvail, Duration.ofMinutes(106)) === None)
  }

  // cross
  test("cross with duration success") {
    val crossableAvail = Availability.fromXml(<availability start="2020-06-10T18:30:00" end="2020-06-10T19:30:00" preference="1"/>).getOrElse(???)
    val excepted = Some(Availability.fromXml(<availability start="2020-06-10T18:30:00" end="2020-06-10T19:30:00" preference="2"/>).getOrElse(???))
    assert(avail1.cross(crossableAvail, Duration.ofMinutes(60)) === excepted)
    assert(crossableAvail.cross(avail1, Duration.ofMinutes(60)) === excepted)
    assert(avail1.cross(avail1, Duration.ofMinutes(60)).getOrElse(???).equals(avail1))
    assert(avail1.cross(crossableAvail, Duration.ofMinutes(61)) === None)
  }

  test("cross between days with duration success") {
    val avail2 = Availability.fromXml(<availability start="2020-06-10T22:00:00" end="2020-06-11T00:30:00" preference="1"/>).getOrElse(???)
    val avail3 = Availability.fromXml(<availability start="2020-06-10T23:00:00" end="2020-06-11T19:30:00" preference="1"/>).getOrElse(???)
    val excepted = Some(Availability.fromXml(<availability start="2020-06-10T23:00:00" end="2020-06-11T00:30:00" preference="2"/>).getOrElse(???))
    assert(avail2.cross(avail3, Duration.ofMinutes(60)) === excepted)
    assert(avail3.cross(avail2, Duration.ofMinutes(90)) === excepted)
    assert(avail3.cross(avail2, Duration.ofMinutes(91)) === None)
  }

  test("cross with duration failure") {
    val uncrossableAvail = Availability.fromXml(<availability start="2020-06-10T19:00:00" end="2020-06-11T19:45:00" preference="1"/>).getOrElse(???)
    assert(avail1.cross(uncrossableAvail, Duration.ofMinutes(90)) === None)
  }
}
