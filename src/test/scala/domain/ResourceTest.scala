package domain

import java.time.Duration

import org.scalatest.funsuite.AnyFunSuite

class ResourceTest extends AnyFunSuite {

  val resource1 = Teacher.fromXml(<teacher id="T001" name="T001">
                      <availability start="2020-06-10T08:00:00" end="2020-06-10T10:05:00" preference="5"/>
                      <availability start="2020-06-12T08:00:00" end="2020-06-12T10:05:00" preference="3"/>
                      <availability start="2020-06-14T08:00:00" end="2020-06-14T10:05:00" preference="1"/>
                      <availability start="2020-06-16T09:00:00" end="2020-06-16T10:00:00" preference="4"/>
                      <availability start="2020-06-16T23:00:00" end="2020-06-17T01:00:00" preference="2"/>
                    </teacher>).getOrElse(???)

  val bAvail1 = Availability.fromXml(<availability start="2020-06-10T08:00:00" end="2020-06-10T09:00:00" preference="5"/>).getOrElse(???) // start
  val bAvail2 = Availability.fromXml(<availability start="2020-06-12T09:05:00" end="2020-06-12T10:05:00" preference="5"/>).getOrElse(???) // end
  val bAvail3 = Availability.fromXml(<availability start="2020-06-14T08:30:00" end="2020-06-14T09:30:00" preference="5"/>).getOrElse(???) // middle
  val bAvail4 = Availability.fromXml(<availability start="2020-06-16T09:00:00" end="2020-06-16T10:00:00" preference="5"/>).getOrElse(???) // equals

  // create
  test("create fromXml failure") {
    assert(Teacher.fromXml(<teacher id="T001" name="001">
                              <availability start="2020-06-10T11:00:00" end="2020-06-10T10:05:00" preference="5"/>
                              <availability start="2020-06-12T08:00:00" end="2020-06-12T10:05:00" preference="3"/>
                              <availability start="2020-06-14T08:00:00" end="2020-06-14T10:05:00" preference="-1"/>
                            </teacher>).isFailure)
  }

  // cross
  test("cross") {
    val bookedAvails = List(bAvail1, bAvail2, bAvail3, bAvail4)
    val duration = Duration.ofMinutes(60)
    val avails = List(Some(Availability.fromXml(<availability start="2020-06-10T09:05:00" end="2020-06-10T10:05:00" preference="5"/>).getOrElse(???)),
                      Some(Availability.fromXml(<availability start="2020-06-10T08:00:00" end="2020-06-10T09:00:00" preference="99"/>).getOrElse(???)),
                      Some(Availability.fromXml(<availability start="2020-06-12T08:00:00" end="2020-06-12T09:00:00" preference="3"/>).getOrElse(???)),
                      Some(Availability.fromXml(<availability start="2020-06-16T23:30:00" end="2020-06-17T00:30:00" preference="2"/>).getOrElse(???)))
    val expected1 = Some(Availability.fromXml(<availability start="2020-06-10T09:05:00" end="2020-06-10T10:05:00" preference="10"/>).getOrElse(???))
    val expected2 = Some(Availability.fromXml(<availability start="2020-06-12T08:00:00" end="2020-06-12T09:00:00" preference="6"/>).getOrElse(???))
    val expected3 = Some(Availability.fromXml(<availability start="2020-06-16T23:30:00" end="2020-06-17T00:30:00" preference="4"/>).getOrElse(???))
    val result = resource1.cross(bookedAvails, avails, duration)
    assert(result.contains(expected1))
    assert(result.contains(expected2))
    assert(result.contains(expected3))
  }

  // getUnbookedAvailabilities
  test("getUnbookedAvailabilities") {
    val bookedAvails = List(bAvail1, bAvail2, bAvail3, bAvail4)
    val expected1 = Availability.fromXml(<availability start="2020-06-10T09:00:00" end="2020-06-10T10:05:00" preference="5"/>).getOrElse(???) // cut start
    val expected2 = Availability.fromXml(<availability start="2020-06-12T08:00:00" end="2020-06-12T09:05:00" preference="3"/>).getOrElse(???) // cut end
    val expected3 = Availability.fromXml(<availability start="2020-06-14T08:00:00" end="2020-06-14T08:30:00" preference="1"/>).getOrElse(???) // cut sides left
    val expected4 = Availability.fromXml(<availability start="2020-06-14T09:30:00" end="2020-06-14T10:05:00" preference="1"/>).getOrElse(???) // cut sides right
    val expected5 = Availability.fromXml(<availability start="2020-06-16T23:00:00" end="2020-06-17T01:00:00" preference="2"/>).getOrElse(???) // no cut
    val expected = List(expected5, expected1,expected2,expected3,expected4)
    val result = resource1.getUnbookedAvailabilities(bookedAvails)
    assert(result === expected)
  }

}
