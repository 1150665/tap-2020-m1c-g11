package assessment

import java.time.{Duration, LocalDateTime, LocalTime}

import domain._
import domain.schedule._

import scala.util.{Failure, Success, Try}
import scala.xml.Elem

object AssessmentMS01 extends Schedule {
  private def firstComeFirstServedAvailability(scheduledVivas: List[ScheduledViva], resources: List[Resource], duration: Duration) : Option[Availability] = {
    if(resources.isEmpty){
      None
    }else{
      val firstResourceBookedAvailabilities = scheduledVivas.filter(_.viva.jury.exists(_.resource.equals(resources(0).id))).map(_.availability)
      val firstResourceAvailabilities = resources(0).getUnbookedAvailabilities(firstResourceBookedAvailabilities)
      val availability = resources.drop(1).foldLeft(firstResourceAvailabilities.map(Option(_))){
        case (lav, r) =>
          val resourceBookedAvailabilities = scheduledVivas.filter(_.viva.jury.exists(_.resource.equals(r.id))).map(_.availability)
          r.cross(resourceBookedAvailabilities,lav,duration)
      }.find(_.isDefined).getOrElse(None)
      if(availability.isDefined){
        val av= Availability.create(availability.get.start,availability.get.start.plus(duration),availability.get.preference)
        av match {
          case Failure(exception) => None
          case Success(av) =>  Some(av)
        }
      }else{
        None
      }
    }
  }
  def scheduleVivas(vivas: List[Viva], resources: List[Resource], duration: Duration): Try[List[ScheduledViva]] = {
    vivas.foldLeft(Try(List[ScheduledViva]())){
      case (sv, v) =>
        if(sv.isSuccess){
          val availability = this.firstComeFirstServedAvailability(
            sv.get,
            resources.filter(r => v.jury.exists(_.resource.equals(r.id))),
            duration)
          if(!availability.isDefined) {
            Failure(new IllegalArgumentException("Viva "+v.title+" cannot be scheduled"))
          } else{
            Success(sv.get :+ ScheduledViva.create(v,availability.get).get)
          }
        }else{
          sv
        }
    }
  }
  // TODO: Use the functions in your own code to implement the assessment of ms01
  def create(xml: Elem): Try[Elem] = {
    val duration = Duration.between(LocalTime.MIN,LocalTime.parse((xml \@ "duration").toString()))
    val vivas = (xml \\ "vivas" \ "viva").map(Viva.fromXml(_)).toList
    val failure = vivas.find(_.isFailure)
    if(failure.isDefined) {
      failure.get match {
        case Failure(exception) => Failure(exception)
      }
    } else {
      val resources = (xml \ "resources" \ "teachers" \ "teacher").map(Teacher.fromXml(_)).toList.appendedAll((xml \ "resources" \ "externals" \ "external").map(External.fromXml(_)).toList)
      val failure = resources.find(_.isFailure)
      if (failure.isDefined) {
        failure.get match {
          case Failure(exception) => Failure(exception)
        }
      } else {
        ScheduledViva.toXml(scheduleVivas(vivas.map(_.get), resources.filter(_.isSuccess).map(_.get), duration), resources.filter(_.isSuccess).map(_.get))
      }
    }
  }
}

object AssessmentMS03 extends Schedule {
  private def maximizedPreferenceAvailability(scheduledVivas: List[ScheduledViva], resources: List[Resource], duration: Duration) : Option[Availability] = {
    if(resources.isEmpty){
      None
    }else{
      val firstResourceBookedAvailabilities = scheduledVivas.filter(_.viva.jury.exists(_.resource.equals(resources.head.id))).map(_.availability)
      val firstResourceAvailabilities = resources.head.getUnbookedAvailabilities(firstResourceBookedAvailabilities)
      val availability = resources.tail.foldLeft(firstResourceAvailabilities.map(Option(_))){
        case (lav, r) =>
          val resourceBookedAvailabilities = scheduledVivas.filter(_.viva.jury.exists(_.resource.equals(r.id))).map(_.availability)
          r.cross(resourceBookedAvailabilities,lav,duration)
      }.filter(_.isDefined).map(_.get).sortBy(av => (av.preference,av.start))(Ordering.Tuple2(Ordering[Int].reverse,Ordering[LocalDateTime])).headOption
      if(availability.isDefined){
        val av= Availability.create(availability.get.start,availability.get.start.plus(duration),availability.get.preference)
        av match {
          case Failure(exception) => None
          case Success(av) =>  Some(av)
        }
      }else{
        None
      }
    }
  }
  private def getAvailabilities(resources: List[Resource], duration: Duration): List[Availability] ={
   resources.tail.foldLeft(resources.head.availabilities.map(Option(_))){
      case (lav, r) =>
        r.cross(List[Availability](),lav,duration)
    }.filter(_.isDefined).map(_.get)
  }
  def scheduleVivas(vivas: List[Viva], resources: List[Resource], duration: Duration): Try[List[ScheduledViva]] = {
    val sortedVivas = vivas.sortBy(v => (v.jury.length,
      this.getAvailabilities(resources.filter(r => v.jury.exists(_.resource.equals(r.id))), duration).map(_.preference).sum,
      v.amountOfVivasWithSharedJury(vivas),
      v.title))(Ordering.Tuple4(Ordering.Int.reverse,Ordering.Int,Ordering.Int,Ordering.String)) // 43/46 RESPOSTAS CERTAS
    val scheduledVivas = sortedVivas.foldLeft(Try(List[ScheduledViva]())){
      case (sv, v) =>
        if(sv.isSuccess){
          val availability = this.maximizedPreferenceAvailability(
            sv.get,
            resources.filter(r => v.jury.exists(_.resource.equals(r.id))),
            duration)
          if(availability.isEmpty) {
            Failure(new IllegalArgumentException("Schedule is impossible"))
          } else{
            Success(sv.get :+ ScheduledViva.create(v,availability.get).get)
          }
        }else{
          sv
        }
    }
    if(scheduledVivas.isSuccess){
      Success(scheduledVivas.get.sortBy(sv => (sv.availability.start, sv.availability.end, sv.viva.title)))
    }else{
      scheduledVivas
    }
  }
  // TODO: Use the functions in your own code to implement the assessment of ms03
  def create(xml: Elem): Try[Elem] = {
    val duration = Duration.between(LocalTime.MIN,LocalTime.parse((xml \@ "duration").toString))
    val vivas = (xml \\ "vivas" \ "viva").map(Viva.fromXml(_)).toList
    val failure = vivas.find(_.isFailure)
    if(failure.isDefined) {
      failure.get match {
        case Failure(exception) => Failure(exception)
      }
    } else {
      val resources = (xml \ "resources" \ "teachers" \ "teacher").map(Teacher.fromXml(_)).toList.appendedAll((xml \ "resources" \ "externals" \ "external").map(External.fromXml(_)).toList)
      val failure = resources.find(_.isFailure)
      if (failure.isDefined) {
        failure.get match {
          case Failure(exception) => Failure(exception)
        }
      } else {
        ScheduledViva.toXml(scheduleVivas(vivas.map(_.get), resources.filter(_.isSuccess).map(_.get), duration), resources.filter(_.isSuccess).map(_.get))
      }
    }
  }
}
