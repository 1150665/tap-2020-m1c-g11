package domain

sealed trait Role

case object President extends Role

case object Adviser extends Role

case object Coadviser extends Role

case object Supervisor extends Role
