package domain

import scala.util.Try
import scala.xml.Elem

final case class Juror private (resource: String, role: Role)
object Juror{
  def create(resource: String, role: Role): Option[Juror] = Some(Juror(resource,role))
  private def convertToRole(roleName: String): Option[Role] ={
    roleName match {
      case "president" => Some(President)
      case "adviser" => Some(Adviser)
      case "coadviser" => Some(Coadviser)
      case "supervisor" => Some(Supervisor)
      case _ => None
    }
  }
  def fromXml(elem: Elem): Option[Juror] = {
    val role = this.convertToRole(elem.label)
    val resource = (elem \@ "id").toString()
    if (!role.isDefined)
      None
    this.create(resource,role.get)
  }
}
