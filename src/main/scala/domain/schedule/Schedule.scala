package domain.schedule

import java.time.Duration

import domain.{Resource, ScheduledViva, Viva}

import scala.util.Try
import scala.xml.Elem

trait Schedule {
  def scheduleVivas(vivas: List[Viva], resources: List[Resource], duration: Duration): Try[List[ScheduledViva]]
  /** Schedule an agenda from xml element
   *
   * @param xml agenda in xml format
   * @return a complete schedule in xml format or an error
   */
  def create(xml: Elem): Try[Elem]
}
