package domain

import java.time.{Duration, LocalDateTime}

import scala.util.{Failure, Success, Try}

final case class Availability private(start: LocalDateTime, end: LocalDateTime, preference: Int){
  def overlaps(availability: Availability): Boolean = {
    availability.start.isAfter(this.start) && availability.start.isBefore(this.end) && availability.end.isAfter(this.end)
  }
  def overlaps(availability: Availability, duration: Duration): Option[Availability] = {
    if(this.overlaps(availability) && Duration.between(availability.start,this.end).compareTo(duration) >= 0){
      val av = Availability.create(availability.start,this.end,this.preference+availability.preference)
      av match {
        case Failure(exception) => None
        case Success(av) =>  Some(av)
      }
    }else{
      None
    }
  }
  def finishedBy(availability: Availability): Boolean = {
    availability.start.isAfter(this.start) && availability.end.isEqual(this.end)
  }
  def finishedBy(availability: Availability, duration: Duration): Option[Availability] = {
    if(this.finishedBy(availability) && Duration.between(availability.start,this.end).compareTo(duration) >= 0){
      val av = Availability.create(availability.start,this.end,this.preference+availability.preference)
      av match {
        case Failure(exception) => None
        case Success(av) =>  Some(av)
      }
    }else{
      None
    }
  }
  def contains(availability: Availability): Boolean = {
    availability.start.isAfter(this.start) && availability.end.isBefore(this.end)
  }
  def contains(availability: Availability, duration: Duration): Option[Availability] = {
    if(this.contains(availability) && Duration.between(availability.start,availability.end).compareTo(duration) >= 0){
      val av = Availability.create(availability.start,availability.end,this.preference+availability.preference)
      av match {
        case Failure(exception) => None
        case Success(av) =>  Some(av)
      }
    }else{
      None
    }
  }
  def starts(availability: Availability): Boolean = {
    availability.start.isEqual(this.start) && availability.end.isAfter(this.end)
  }
  def starts(availability: Availability, duration: Duration): Option[Availability] = {
    if(this.starts(availability) && Duration.between(availability.start,this.end).compareTo(duration) >= 0){
      val av = Availability.create(availability.start,this.end,this.preference+availability.preference)
      av match {
        case Failure(exception) => None
        case Success(av) =>  Some(av)
      }
    }else{
      None
    }
  }
  def equals(availability: Availability): Boolean = {
    availability.start.isEqual(this.start) && availability.end.isEqual(this.end)
  }
  def equals(availability: Availability, duration: Duration): Option[Availability] = {
    if(this.equals(availability) && Duration.between(availability.start,this.end).compareTo(duration) >= 0){
      val av = Availability.create(availability.start,this.end,this.preference+availability.preference)
      av match {
        case Failure(exception) => None
        case Success(av) =>  Some(av)
      }
    }else{
      None
    }
  }
  def cross(availability: Availability, duration: Duration): Option[Availability] = {
    List(this.overlaps(availability,duration), this.finishedBy(availability,duration), this.contains(availability, duration),
      this.starts(availability, duration), this.equals(availability,duration),availability.overlaps(this,duration),
      availability.finishedBy(this,duration),availability.contains(this, duration),
      availability.starts(this, duration)).find(_.isDefined).getOrElse(None)
  }
}
object Availability{
  private val isPreferenceValid: (Int) => Boolean = (preference) => preference >= 0
  private val areDatesValid: (LocalDateTime, LocalDateTime) => Boolean = (start, end) => start.isBefore(end)
  private val isValid: (LocalDateTime, LocalDateTime, Int) => Boolean = (start, end,preference) => areDatesValid(start,end) && isPreferenceValid(preference)
  def create(start: LocalDateTime, end: LocalDateTime, preference: Int): Try[Availability] = if(isValid(start,end,preference))
    Success(Availability(start, end, preference)) else Failure(new IllegalArgumentException("Availability start cannot be after end"))
  def fromXml(node: scala.xml.Node): Try[Availability] = {
    val start = LocalDateTime.parse((node \@ "start").toString())
    val end = LocalDateTime.parse((node \@ "end").toString())
    val preference = (node \@ "preference").toString().toInt
    this.create(start,end,preference)
  }
}
