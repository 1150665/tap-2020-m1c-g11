package domain.util

import scala.util.{Failure, Success, Try}

final case class NonEmptyString(s: String) extends AnyVal
object NonEmptyString {

  def from (s: String, element: String): Try[NonEmptyString] = {
    if (!s.isEmpty) Success(NonEmptyString(s))
    else Failure(new Exception(element))
  }
}
