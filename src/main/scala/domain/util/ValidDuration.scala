package domain.util

import java.time.{Duration, LocalTime}

import scala.util.{Failure, Success, Try}

final case class ValidDuration(val v: java.time.Duration) extends AnyVal

object ValidDuration {

  def fromString(v: String): Try[ValidDuration] = {
    if (v.toInt <= 0)  Failure(new Exception(s"Duration value $v is not valid"))
    else Success(ValidDuration(java.time.Duration.between(LocalTime.MIN,LocalTime.parse(v))))
  }
}
