package domain

import scala.util.{Failure, Success, Try}
import scala.xml.Elem

final case class Viva private (title: String, student: String, jury: List[Juror]){
  val president = jury.find(_.role.equals(President)).get
  val adviser = jury.find(_.role.equals(Adviser)).get
  val coadvisers = jury.filter(_.role.equals(Coadviser))
  val supervisors = jury.filter(_.role.equals(Supervisor))

  def amountOfVivasWithSharedJury(vivas: List[Viva]): Int = {
    vivas.filter(!_.equals(this)).foldLeft(0){
      case (amount, v) =>
        val isShared = this.jury.foldLeft(false){
          case (exists,j) =>
            if(v.jury.exists(_.resource.equals(j.resource))){
              true
            }else{
              exists
            }
        }
        if(isShared){
          amount+1
        }else{
          amount
        }
    }
  }
}
object Viva{
  private val hasOnePresident: List[Juror] => Boolean = _.filter(j => j.role.equals(President)).size == 1
  private val hasOneAdviser: List[Juror] => Boolean = _.filter(j => j.role.equals(Adviser)).size == 1
  def create(title: String, student: String, jury: List[Juror]): Try[Viva] = {
    if(!hasOnePresident(jury)) {
      Failure(new IllegalArgumentException("Node president is empty/undefined in viva"))
    } else if(!hasOneAdviser(jury)) {
      Failure(new IllegalArgumentException("Node adviser is empty/undefined in viva"))
    } else {
      Success(Viva(title,student,jury))
    }
  }
  def fromXml(node: scala.xml.Node): Try[Viva] = {
    val title = (node \@ "title").toString()
    val student = (node \@ "student").toString()
    val jury = node.child.collect {case e: Elem => e}.map(Juror.fromXml(_))
    this.create(title, student, jury.filter(_.isDefined).map(_.get).toList)
  }
}
