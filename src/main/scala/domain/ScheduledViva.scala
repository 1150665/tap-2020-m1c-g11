package domain

import java.time.format.DateTimeFormatter

import scala.util.{Failure, Success, Try}
import scala.xml.Elem

final case class ScheduledViva private (viva: Viva, availability: Availability){
  final val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")
  def toXml(resources: List[Resource]): Elem = {
      <viva student={this.viva.student} title={this.viva.title}
            start={this.availability.start.format(this.formatter)} end={this.availability.end.format(this.formatter)} preference={this.availability.preference.toString}>
        <president name={resources.find(_.id.equals(this.viva.president.resource)).get.name}/>
        <adviser name={resources.find(_.id.equals(this.viva.adviser.resource)).get.name}/>
        {this.viva.coadvisers.map(x => <coadviser name={resources.find(_.id.equals(x.resource)).get.name}/>)}
        {this.viva.supervisors.map(x => <supervisor name={resources.find(_.id.equals(x.resource)).get.name}/>)}
      </viva>
  }
}
object ScheduledViva{
  def create(viva: Viva, availability: Availability): Try[ScheduledViva] = Success(ScheduledViva(viva,availability))
  def toXml(scheduledVivas: Try[List[ScheduledViva]], resources: List[Resource]): Try[Elem] = {
    scheduledVivas match {
      case Failure(exception) => Failure(exception)
      case Success(sv) => Success(
        <schedule xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="../../schedule.xsd" totalPreference={sv.map(_.availability.preference).sum.toString}>
          {sv.map(_.toXml(resources))}
        </schedule>)
    }
  }
}
