package domain

import java.time.Duration

import scala.util.{Failure, Try}
import scala.xml.Elem

abstract class Resource (val id: String, val name: String, val availabilities: List[Availability]){
  def cross(bookedAvailabilities: List[Availability], availabilities: List[Option[Availability]],duration: Duration): List[Option[Availability]] = {
    val unbookedAvailabilities = this.getUnbookedAvailabilities(bookedAvailabilities)
    availabilities.filter(_.isDefined).map(_.get).foldLeft(List[Option[Availability]]()){
      case (lav, av) =>
        lav ::: unbookedAvailabilities.map(av.cross(_,duration))
    }
  }
  def getUnbookedAvailabilities(bookedAvailabilities: List[Availability]): List[Availability] = {
    bookedAvailabilities.foldLeft(this.availabilities){
      case (lav, bav) =>
        val lavToSplit = lav.filter(av => av.finishedBy(bav) || av.contains(bav) || bav.starts(av) || av.equals(bav))
        lavToSplit.foldLeft(lav){
          case (lavSplitted, avToSplit) =>
            lavSplitted.patch(lavSplitted.indexOf(avToSplit),Nil,1) ::: List(Availability.create(avToSplit.start,bav.start,avToSplit.preference),
              Availability.create(bav.end,avToSplit.end,avToSplit.preference)).filter(_.isSuccess).map(_.get)
        }
        /*if(avToSplit.isDefined){
          lav.patch(lav.indexOf(avToSplit.get),Nil,1) ::: List(Availability.create(avToSplit.get.start,bav.start,avToSplit.get.preference),
            Availability.create(bav.end,avToSplit.get.end,avToSplit.get.preference)).filter(_.isSuccess).map(_.get)
        }else{
          lav
        }*/
    }
  }
}

trait ResourceFactory{
  def create(id: String, name: String, availabilities: List[Availability]): Try[Resource]
  def fromXml(node: scala.xml.Node): Try[Resource] = {
    val id = (node \@ "id").toString()
    val name = (node \@ "name").toString()
    val availabilities = node.child.collect {case e: Elem => e}.map(Availability.fromXml(_))
    val failure = availabilities.find(_.isFailure)
    if(failure.isDefined) {
      failure.get match {
        case Failure(exception) => Failure(exception)
      }
    } else {
      this.create(id, name, availabilities.filter(_.isSuccess).map(_.get).toList)
    }
  }
}