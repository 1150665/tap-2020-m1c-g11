package domain

import scala.util.{Success, Try}

final case class External private (override val id: String, override val name: String, override val availabilities: List[Availability]) extends Resource(id,name,availabilities) {

}
object External extends ResourceFactory {
  override def create(id: String, name: String, availabilities: List[Availability]): Try[External] = Success(External(id,name,availabilities))
}
