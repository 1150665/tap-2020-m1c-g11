package domain

import scala.util.{Success, Try}

final case class Teacher private (override val id: String, override val name: String, override val availabilities: List[Availability]) extends Resource(id,name,availabilities) {

}
object Teacher extends ResourceFactory {
  override def create(id: String, name: String, availabilities: List[Availability]): Try[Teacher] = Success(Teacher(id,name,availabilities))
}